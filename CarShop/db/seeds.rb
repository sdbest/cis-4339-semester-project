# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Car.delete_all
car_list = %w{Acura Z37 Black 2015 1234ABCD 40000},
    %w{Ascari V6 Black 2014 1234ABED 35000},
    %w{AstonMartin Slayer White 2012 1234ABCE 60800},
    %w{BMW MKS Black 2015 1983BUYH 98000},
    %w{Candilac Escalade Black 2010 760JHYG 25000},
    %w{Chervolet Covette Brown 2013 9807BAPU 29000},
    %w{Fearri 250 Black 2005 0987IHNG 320400},
    %w{Ford F150 White 2011 9800KMJL 24200},
    %w{GMC Serria Silver 2011 9899IIJU 57000},
    %w{Honda Civic Black 2005 0911KKHN 45000},
    %w{Huyndai Tuberion Blue 2008 1199KLOU 23400},
    %w{Infinity G35 Green 201 1183BBAG 46000},
    %w{Kia Serento Black 2015 1083BALOY 21700},
    %w{Lincoln Navigator Red 2013 1234OJBY 34400},
    %w{Nissan Altima Silver 2013 0987BATE 28000},
    %w{Porsche 911 Blue 2011 1872KHOA 135000},
    %w{Subaru Impreza Black 2014 9999NHIN 72300},
    %w{Toyota Corolla Black 2015 2222HHHH 33500}

    car_list.each do | m,o,c,y,v,p |

      Car.create(model:m , make:o, color:c, year:y ,vin:v, price:p ,car_status:0)
    end


SalePerson.delete_all
SalePerson.create( :name => "Jason")
SalePerson.create( :name => "John")
SalePerson.create( :name => "Kim")
SalePerson.create( :name => "Shane")
SalePerson.create( :name => "Trevor")
SalePerson.create( :name => "Bob")


Customer.delete_all
customer_list = [
    [ "Shawn Best", " 17503 Bon Sean dr ", "Houston",  "TX", "44536", "2819821134", "mrshawn_best@gmail.com" ],
    [ "John Clark", "1245 Perry st", "Katy", "TX", "45102", "8328761134", "johndon@hotmail.com" ],
    [ "David Blane", "2511 Colony Rd", "Houston", "TX", "77083","2812234123", "davblade@yahoo.com" ],
    [ "Don John", "4812 Bellair Rd", "Sugarland", "TX", "77183","832114567","donwang@gmail.com" ],
    [ "Tom Brady", "9504 Boon Rd", "SugarLand", "TX","77913", "832114567", "Tombrady@yahoo.com" ],
    [ "Micheal Hussey", "4002 King Rd", "Katy", "TX","77818", "832114567", "husslehard@hotmail.com" ],

]



customer_list.each do |n,a,c,s,z,p,e|
  Customer.create( name: n, address:a, city: c, state: s, zip: z , phone:p, email: e )
end