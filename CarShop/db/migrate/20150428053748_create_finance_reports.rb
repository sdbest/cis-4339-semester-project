class CreateFinanceReports < ActiveRecord::Migration
  def change
    create_table :finance_reports do |t|

      t.timestamps null: false
    end
  end
end
