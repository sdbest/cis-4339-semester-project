class CreateLoans < ActiveRecord::Migration
  def change
    create_table :loans do |t|
      t.integer :quote_id
      t.float :down_payment
      t.float :borrowed_amount
      t.float :interest
      t.integer :term
      t.float :monthly_payment
      t.boolean :loan_status

      t.timestamps null: false
    end
  end
end
