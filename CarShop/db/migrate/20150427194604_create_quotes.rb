class CreateQuotes < ActiveRecord::Migration
  def change
    create_table :quotes do |t|
      t.integer :sale_person_id
      t.integer :customer_id
      t.integer :car_id
      t.float :car_price
      t.float :sale_tax
      t.float :total_price
      t.boolean :quote_status

      t.timestamps null: false
    end
  end
end
