class CreateCars < ActiveRecord::Migration
  def change
    create_table :cars do |t|
      t.string :model
      t.string :make
      t.string :color
      t.integer :year
      t.string :vin
      t.float :price
      t.boolean :car_status

      t.timestamps null: false
    end
  end
end
