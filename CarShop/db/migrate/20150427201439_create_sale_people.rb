class CreateSalePeople < ActiveRecord::Migration
  def change
    create_table :sale_people do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
