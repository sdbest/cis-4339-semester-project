class CreateCarStocks < ActiveRecord::Migration
  def change
    create_table :car_stocks do |t|

      t.timestamps null: false
    end
  end
end
