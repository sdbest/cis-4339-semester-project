require 'test_helper'

class SalesmanQuotesControllerTest < ActionController::TestCase
  setup do
    @salesman_quote = salesman_quotes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:salesman_quotes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create salesman_quote" do
    assert_difference('SalesmanQuote.count') do
      post :create, salesman_quote: {  }
    end

    assert_redirected_to salesman_quote_path(assigns(:salesman_quote))
  end

  test "should show salesman_quote" do
    get :show, id: @salesman_quote
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @salesman_quote
    assert_response :success
  end

  test "should update salesman_quote" do
    patch :update, id: @salesman_quote, salesman_quote: {  }
    assert_redirected_to salesman_quote_path(assigns(:salesman_quote))
  end

  test "should destroy salesman_quote" do
    assert_difference('SalesmanQuote.count', -1) do
      delete :destroy, id: @salesman_quote
    end

    assert_redirected_to salesman_quotes_path
  end
end
