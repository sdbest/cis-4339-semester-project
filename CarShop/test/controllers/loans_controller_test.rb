require 'test_helper'

class LoansControllerTest < ActionController::TestCase
  setup do
    @loan = loans(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:loans)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create loan" do
    assert_difference('Loan.count') do
      post :create, loan: { borrowed_amount: @loan.borrowed_amount, down_payment: @loan.down_payment, interest: @loan.interest, loan_status: @loan.loan_status, monthly_payment: @loan.monthly_payment, quote_id: @loan.quote_id, term: @loan.term }
    end

    assert_redirected_to loan_path(assigns(:loan))
  end

  test "should show loan" do
    get :show, id: @loan
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @loan
    assert_response :success
  end

  test "should update loan" do
    patch :update, id: @loan, loan: { borrowed_amount: @loan.borrowed_amount, down_payment: @loan.down_payment, interest: @loan.interest, loan_status: @loan.loan_status, monthly_payment: @loan.monthly_payment, quote_id: @loan.quote_id, term: @loan.term }
    assert_redirected_to loan_path(assigns(:loan))
  end

  test "should destroy loan" do
    assert_difference('Loan.count', -1) do
      delete :destroy, id: @loan
    end

    assert_redirected_to loans_path
  end
end
