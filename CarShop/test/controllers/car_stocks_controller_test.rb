require 'test_helper'

class CarStocksControllerTest < ActionController::TestCase
  setup do
    @car_stock = car_stocks(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:car_stocks)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create car_stock" do
    assert_difference('CarStock.count') do
      post :create, car_stock: {  }
    end

    assert_redirected_to car_stock_path(assigns(:car_stock))
  end

  test "should show car_stock" do
    get :show, id: @car_stock
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @car_stock
    assert_response :success
  end

  test "should update car_stock" do
    patch :update, id: @car_stock, car_stock: {  }
    assert_redirected_to car_stock_path(assigns(:car_stock))
  end

  test "should destroy car_stock" do
    assert_difference('CarStock.count', -1) do
      delete :destroy, id: @car_stock
    end

    assert_redirected_to car_stocks_path
  end
end
