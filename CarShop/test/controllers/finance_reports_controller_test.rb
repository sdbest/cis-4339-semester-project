require 'test_helper'

class FinanceReportsControllerTest < ActionController::TestCase
  setup do
    @finance_report = finance_reports(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:finance_reports)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create finance_report" do
    assert_difference('FinanceReport.count') do
      post :create, finance_report: {  }
    end

    assert_redirected_to finance_report_path(assigns(:finance_report))
  end

  test "should show finance_report" do
    get :show, id: @finance_report
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @finance_report
    assert_response :success
  end

  test "should update finance_report" do
    patch :update, id: @finance_report, finance_report: {  }
    assert_redirected_to finance_report_path(assigns(:finance_report))
  end

  test "should destroy finance_report" do
    assert_difference('FinanceReport.count', -1) do
      delete :destroy, id: @finance_report
    end

    assert_redirected_to finance_reports_path
  end
end
