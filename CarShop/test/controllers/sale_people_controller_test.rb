require 'test_helper'

class SalePeopleControllerTest < ActionController::TestCase
  setup do
    @sale_person = sale_people(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:sale_people)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create sale_person" do
    assert_difference('SalePerson.count') do
      post :create, sale_person: { name: @sale_person.name }
    end

    assert_redirected_to sale_person_path(assigns(:sale_person))
  end

  test "should show sale_person" do
    get :show, id: @sale_person
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @sale_person
    assert_response :success
  end

  test "should update sale_person" do
    patch :update, id: @sale_person, sale_person: { name: @sale_person.name }
    assert_redirected_to sale_person_path(assigns(:sale_person))
  end

  test "should destroy sale_person" do
    assert_difference('SalePerson.count', -1) do
      delete :destroy, id: @sale_person
    end

    assert_redirected_to sale_people_path
  end
end
