json.array!(@quotes) do |quote|
  json.extract! quote, :id, :sale_person_id, :customer_id, :car_id, :car_price, :sale_tax, :total_price, :quote_status
  json.url quote_url(quote, format: :json)
end
