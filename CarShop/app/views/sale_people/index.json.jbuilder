json.array!(@sale_people) do |sale_person|
  json.extract! sale_person, :id, :name
  json.url sale_person_url(sale_person, format: :json)
end
