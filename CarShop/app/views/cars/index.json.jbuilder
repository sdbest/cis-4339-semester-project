json.array!(@cars) do |car|
  json.extract! car, :id, :model, :make, :color, :year, :vin, :price, :car_status
  json.url car_url(car, format: :json)
end
