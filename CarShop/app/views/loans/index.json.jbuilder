json.array!(@loans) do |loan|
  json.extract! loan, :id, :quote_id, :down_payment, :borrowed_amount, :interest, :term, :monthly_payment, :loan_status
  json.url loan_url(loan, format: :json)
end
