json.extract! @loan, :id, :quote_id, :down_payment, :borrowed_amount, :interest, :term, :monthly_payment, :loan_status, :created_at, :updated_at
