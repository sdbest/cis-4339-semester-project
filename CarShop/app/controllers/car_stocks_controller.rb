class CarStocksController < ApplicationController

  def index

    if params[:search]
      @cars = Car.search_car(params[:search],params[:search_column]).paginate(:page => params[:page], :per_page => 10)
    else
      @cars = Car.all.paginate(:page => params[:page], :per_page => 10)
    end
  end

end
