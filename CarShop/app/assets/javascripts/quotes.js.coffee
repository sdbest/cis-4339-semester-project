# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

updateQuote = ->
  selection_id = $('#quote_car_id').val()
  $.getJSON '/cars/' + selection_id + '/cal_market_price', {},(json, response) ->

    msrp = json['price']

    markupprice= (msrp * 1.1).toFixed(2)

    $('#quote_car_price').val(markupprice)

    SaleTax = (markupprice * 0.043).toFixed(2)

    $('#quote_sale_tax').val(SaleTax)

    Total = (parseFloat(markupprice) + parseFloat(SaleTax)).toFixed(2)

    $('#quote_total_price').val(Total)

$ ->
  $('#quote_car_id').change -> updateQuote()



