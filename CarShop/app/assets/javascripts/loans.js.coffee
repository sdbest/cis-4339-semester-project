# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

updateBorrow = ->
  quoteid = $('#loan_quote_id').val()
  $.getJSON '/quotes/' + quoteid + '/call_quote_price', {},(json, response) ->

    quoteprice = json['total_price']

    downpayment = $('#loan_down_payment').val()

    borrow = (quoteprice-downpayment).toFixed(2)

    $('#loan_borrow_amount').val(borrow)

$ ->
  $('#loan_down_payment').change -> updateBorrow()


# use ajax to get borrow amount above and use it to call monthly payment

updateMonthlyPayment = ->

  amount_borrow = $('#loan_borrow_amount').val()

  rate = $('#loan_interest').val()

  loan_term = $('#loan_term').val()


  # equation to calculate monthly payment

  r = rate / 1200
  n = r * amount_borrow
  d = 1 - (1 + r)**-((loan_term*12))
  payment = (n/d).toFixed(2)

  $('#loan_monthly_payment').val(payment)


$ ->
  $('#loan_term').change -> updateMonthlyPayment()