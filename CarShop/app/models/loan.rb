class Loan < ActiveRecord::Base
  belongs_to :quote

  #validation
  validates :down_payment ,presence: true
  validates :interest ,presence: true


  def status_loan
    if loan_status == true

      self.loan_status=:Approved

    else
      self.loan_status=:NotApproved

    end
  end
end

