class Quote < ActiveRecord::Base
  belongs_to :customer
  belongs_to :sale_person
  belongs_to :car
  has_one :loan



  def quote_name
    " Customer name: #{customer.name} \n
      Quote Price:$#{total_price} "
  end


  def update_status
    if self.quote_status == true
      car.update_attribute(:car_status,true)
      self.quote_status=:sold

    else
      car.update_attribute(:car_status,false)
      self.quote_status=:unsold

    end

  end

  def final_price
    self.total_price = (self.car.price*1.1* 1.043).round(2)
  end

private
  def self.search(search)
    if search
      joins(:sale_person).where(' name LIKE ? ', "%#{search}%")
    else
      find(:all)
    end
  end
end




