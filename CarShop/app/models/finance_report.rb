class FinanceReport < ActiveRecord::Base
  def gross
    where('quote_status = ?',true).sum(:total_price)
  end

  def profit
    where('quote_status = ?',true).sum(:total_price) - where('quote_status = ?',true).sum(:car_price)
  end

  def sale_tax
    where('quote_status = ?',true).sum(:sale_tax)
  end
end
