class Car < ActiveRecord::Base
  has_many :quotes


  validates :model ,presence: true
  validates :color ,presence: true
  validates :vin ,presence: true
  validates :price ,presence: true

  def car_info
    "#{model},  Msrp price:#{price}$ "
  end



  def car_update_status
    if self.car_status == true
      self.car_status=:sold

    else
      self.car_status=:unsold

    end
  end


  def self.searchable_columns_car
    wanted_columns = ['model', 'color', 'vin' ]
    self.column_names.select{ |column| wanted_columns.include?(column) }
  end


  def self.translated_columns_car
    columns = self.searchable_columns_car
    result = columns.map{ |column| [Car.human_attribute_name(column.to_sym), column] }
    result
  end


  def self.search_car(keyword,column_name)
    if self.column_names.include?(column_name.to_s)
      where("cars.#{column_name} LIKE ?", "%#{keyword}%")
    else
      self.all
    end
  end

  def cal_markup
     markup = (self.price * 1.1).round(2)
  end



end