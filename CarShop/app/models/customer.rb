class Customer < ActiveRecord::Base
  has_many :quotes
  validates :name ,presence: true

  def self.searchable_columns
    wanted_columns = ['name', 'address', 'phone' ]
    self.column_names.select{ |column| wanted_columns.include?(column) }
  end


  def self.translated_searchable_columns
    columns = self.searchable_columns
    result = columns.map{ |column| [Customer.human_attribute_name(column.to_sym), column] }
    result
  end


  def self.search(keyword,column_name)
    if self.column_names.include?(column_name.to_s)
      where("customers.#{column_name} LIKE ?", "%#{keyword}%")
    else
      self.all
    end
  end
end
